package com.copplest.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import org.springframework.stereotype.Service;

import com.copplest.model.Note;
import com.copplest.model.NoteTable;

@Service
public class CalcService {
    static ArrayList<Note> oldNoteTable =  NoteTable.getBlankBankNoteTable();
    static ArrayList<Note> newNoteTable =  NoteTable.getBlankBankNoteTable();

    public static ArrayList <Note> calculateNewDenomination(double amount){
       

        ArrayList<Note> resultTable = NoteTable.getBlankBankNoteTable();
        double rest = 0.0;
        double restRounded = 0.0;
        try{
            for (Note entry : resultTable ){
                rest = amount % entry.getValue();
                
                BigDecimal bd = new BigDecimal(rest).setScale(2, RoundingMode.HALF_UP);
                restRounded = bd.doubleValue();

                double diff = (amount - restRounded);
                double noteValue = entry.getValue();
                double valueCount  = diff/noteValue;

                BigDecimal cbd = new BigDecimal(valueCount).setScale(2, RoundingMode.HALF_UP);
                int count = cbd.intValue();
                                
                if (count > 0){
                    entry.setCount(count);
                };
                amount = restRounded;
            };
        }catch (Exception e){
            System.out.println(e.getMessage());
        }

        newNoteTable = resultTable;
        
        return resultTable;
    };
    
    public static ArrayList <Note> calculateDifferences(){
        ArrayList<Note> diffTable = new ArrayList<Note>();
        
        try{
            for ( int index = 0; index < newNoteTable.size(); index++){
                Note newEntry = newNoteTable.get(index);  
                Note oldEntry = oldNoteTable.get(index);             
                            
                int diff = newEntry.getCount() - oldEntry.getCount();
                
                Note newNote = new Note(newEntry.getValue());
                if(diff>0){
                    newNote.setSign("+"); 
                }
                newNote.setCount(diff);
                diffTable.add(newNote);
            }
        }catch (Exception e){
            System.out.println(e.getMessage());
        }

        return diffTable;
    };

    public static void setOldTable(ArrayList<Note> oldTable){
        oldNoteTable = oldTable;
    }
}
