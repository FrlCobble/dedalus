package com.copplest.controller;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.*;
import com.copplest.model.Note;
import com.copplest.service.CalcService;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.Gson;
import java.lang.reflect.Type;
import com.google.gson.reflect.TypeToken;

@SpringBootApplication
@RestController
@RequestMapping("/api")
public class DedalusController {
	@CrossOrigin
	@GetMapping("/getDenomination")
	public ResponseEntity <List<Note>> getDenomination(@RequestParam("data") Double newAmount){
		System.out.println("amount:" + newAmount);
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Custom-Header", "Custom-Value");

        return new ResponseEntity<>(CalcService.calculateNewDenomination(newAmount), headers, HttpStatus.OK);
	}
	@CrossOrigin
	@GetMapping("/getDifferences")	
	public ResponseEntity <List<Note>> getDifferenceTable(){
		
		HttpHeaders headers = new HttpHeaders();
		headers.add("Custom-Header", "Custom-Value");
		
		List<Note> returnNotes = CalcService.calculateDifferences();

        return new ResponseEntity<>(returnNotes, headers, HttpStatus.OK);
	}
	@CrossOrigin
	@PostMapping("/setOldTable")	
	public  void setOldTable(@RequestBody String oldTableString){
		System.out.println("oldTable Content:" + oldTableString);

		Type listType = new TypeToken<ArrayList<Note>>() {}.getType();
		Gson gson = new Gson();
        ArrayList<Note> listOldNotes = gson.fromJson(oldTableString, listType);

		CalcService.setOldTable(listOldNotes);

	}

	public static void main(String[] args) {
		SpringApplication.run(DedalusController.class, args);
	}

}
 