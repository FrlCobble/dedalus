package com.copplest.model;

public class Note  {
    private Double value;
    private String sign;
    private Integer count = 0;
    
    public Note(){}
    public Note(Double value) {
        this.value = value;     
        this.count = 0;   
    }
    public Note(Double value, Integer count) {
        this.value = value;
        this.count = count;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }
    
    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count2) {
        this.count = count2;
    }

    public String getSign(){
        return this.sign;
    }
    public void setSign(String sign){
        this.sign = sign;        
    }

}
