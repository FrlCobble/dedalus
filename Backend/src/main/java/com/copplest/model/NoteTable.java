package com.copplest.model;
import java.util.ArrayList;

public class NoteTable {
    public static ArrayList<Note> getBlankBankNoteTable(){
        ArrayList<Note> notes = new ArrayList<>(); 
        notes.add(new Note(200.0));
        notes.add(new Note(100.0));
        notes.add(new Note(50.0));
        notes.add(new Note(20.0));
        notes.add(new Note(10.0));
        notes.add(new Note(5.0));
        notes.add(new Note(2.0));
        notes.add(new Note(1.0));
        notes.add(new Note(0.50));
        notes.add(new Note(0.20));
        notes.add(new Note(0.10));
        notes.add(new Note(0.05));
        notes.add(new Note(0.02));
        notes.add(new Note(0.01));

        return notes;
    }
    
}
