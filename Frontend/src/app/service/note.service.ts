import { Injectable } from '@angular/core';
import { Note } from '../class/note';


@Injectable({
  providedIn: 'root'
})
export class NoteService {
  static diffTable: any;

  constructor() {
  }

  //creates an empty array with available banknotes
  static getArrayOfBanknotes():Array<Note>{
    let banknotes :Array<Note> = new Array<Note>();
    banknotes.push(new Note(200.0), 
                  new Note(100.0),
                  new Note(50.0),
                  new Note(20.0),
                  new Note(10.0),
                  new Note(5.0),
                  new Note(2.0),
                  new Note(1.0),
                  new Note(0.50),
                  new Note(0.20),
                  new Note(0.10),
                  new Note(0.05),
                  new Note(0.02),
                  new Note(0.01));
    return banknotes;
  }

  //rounds a number to two decimal places
  static roundToTwo(num:number):number {
    var m = Number((Math.abs(num) * 100).toPrecision(15));
    return Math.round(m) / 100 * Math.sign(num);
  }

  
  static doCalculation(amount:number):Array<Note>{
    let resultTable = this.getArrayOfBanknotes();
    let rest=0.0;
    let restRounded = 0.0;
        
    resultTable.forEach(function(entry){
       
      rest = amount % entry.value;
      restRounded = NoteService.roundToTwo(rest);
      let a = Math.round((amount - restRounded)/ entry.value);
       
      if (a > 0){
        entry.count = a;
      }
       
      console.log(entry);
      amount = restRounded;
            
    })
    
    return resultTable;
  }

  static calculateDifferences(newNotes: Array<Note>, oldNotes: Array<Note>):Array<Note> {
    let diffTable:Array<Note> = new Array<Note>();
    
    for (let index = 0; index < newNotes.length; index++){
      let newEntry = newNotes[index];
      let oldEntry = oldNotes[index];

      let difference = newEntry.count - oldEntry.count;
      
      let noteValue = new Note(newEntry.value);
      if(difference > 0){
        noteValue.sign = "+";
      }
      noteValue.count = difference;
      diffTable.push(noteValue);

    }

    return diffTable;    
  }

}