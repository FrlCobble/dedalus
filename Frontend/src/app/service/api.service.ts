import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Note } from '../class/note';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
  })
  export class ApiService {
    url:string = 'http://localhost:8080/api/';       

    constructor(private http:HttpClient) {
    }

    calculateDenominationInBackend(amount:number):Observable<Object>{                      
        return this.http.get(this.url + "getDenomination", { params: { data: amount } });       
    }

    setOldTable(oldtable:Array<Note>){        
        let jsonstring = JSON.stringify(oldtable)
        return this.http.post<Array<Note>>(this.url + "setOldTable", jsonstring);
    }

    calculateDifferencesInBackend():Observable<Object>{                          
        return this.http.get(this.url + "getDifferences");                                                   
    }
}