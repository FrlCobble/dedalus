import { LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import {HttpClientModule} from '@angular/common/http';
import { DecimalPipe } from '@angular/common';
import { CustomNumberPipe } from './pipes/custumnumber.pipe';
import { registerLocaleData } from '@angular/common';
import localeDe from '@angular/common/locales/de';
import localeDeExtra from '@angular/common/locales/extra/de';

registerLocaleData(localeDe, 'de-DE', localeDeExtra);

@NgModule({
  declarations: [
    AppComponent  ,
    CustomNumberPipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [
    {provide: LOCALE_ID, useValue: 'de-DE'},
    DecimalPipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
