
import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { ApiService } from './service/api.service';
import { NoteService } from './service/note.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
   
  useFrontend=true;    
  title = 'Dedalus';
  newAmount = 0.0;
  oldAmount =0.0;
  showOldAmount=0.0;
  oldNotes:any;
  newNotes:any;
  diffNotes:any;

  constructor(private api :ApiService,private http:HttpClient) { }

  ngOnInit() {  
    this.newNotes = NoteService.getArrayOfBanknotes();    
    this.diffNotes = NoteService.getArrayOfBanknotes();    
    this.oldNotes = NoteService.getArrayOfBanknotes();
  }
  onSubmit(inputNewValue: string) {         
    //cache old values
    this.oldNotes = this.newNotes;
    this.oldAmount = this.newAmount;

    //replace eu decimal comma with decimal point
    let inputNum:number = 0.0;
    inputNum = parseFloat(inputNewValue.replace(/,/g, '.'));

    //show amount on webpage
    this.newAmount  = inputNum;        
 
    if (this.useFrontend){
      //calculate denomination in the frontend
      this.newNotes= NoteService.doCalculation(this.newAmount);
      this.diffNotes = NoteService.calculateDifferences(this.newNotes,this.oldNotes);
    }else{
      //perform the calculation on the backend server
      this.api.setOldTable(this.oldNotes).subscribe(data=>{
        console.log(data);
        this.api.calculateDenominationInBackend(this.newAmount).subscribe(data=>{
          this.newNotes = data;
          this.api.calculateDifferencesInBackend().subscribe({
            next:(data)=>this.diffNotes = data,
            error: (e)=>console.log(e)
          });
        }); 
      });
  
    }    
  }
 
  //switch between frontend and backend calculation
  onCheckboxChange(e:any) {
    this.useFrontend=!this.useFrontend;
  }
}
