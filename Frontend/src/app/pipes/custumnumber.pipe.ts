import { PipeTransform, Pipe } from "@angular/core";


@Pipe({name: 'customNumber'})
export class CustomNumberPipe implements PipeTransform {
    transform(value: number): any {
        var numberValue = value.toFixed(2);
        numberValue = numberValue.replace(/\./g, ",").toString();

        return numberValue;
    }

}
